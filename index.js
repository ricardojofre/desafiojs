function api(){
  
    const fetch=require('node-fetch');
    const status=response=>{
        if(response.status>=200 && response.status<300){
            return Promise.resolve(response);//se pasa al siguiente eslavon de la cadena 
    
        }
        return Promise.reject(new Error(response.statusText));
    
    };
    
    const obtenerJSON=response =>{
        return response.json();
    }
    
    fetch('https://mindicador.cl/api')
    .then(status)
    .then(obtenerJSON)
    .then(console.log)
}